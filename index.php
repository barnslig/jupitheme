<?php get_header(); ?>

<?php while (have_posts()) : the_post() ?>
	<article>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
			<div class="content">
				<?php if(!is_page()): ?>
					<div class="meta">
						<?php the_author_link(); ?>  – <?php the_category(', '); ?> – <?php the_date('d. m. Y'); ?>
					</div>
				<?php endif; ?>
				<?php the_content(); ?>

				<?php if(!is_single() && !is_page()) comments_popup_link('<i class="icon-plus-sign"></i> kommentieren', '<i class="icon-comment"></i> ein Kommentar', '<i class="icon-comments"></i> % Kommentare', 'comment-button btn'); ?>
				<?php if(is_single() && !is_page()) { comments_template(); } ?>
			</div>
		</div>
	</article>
<?php
	endwhile;

	if(is_search() && !have_posts()) {
		echo "<article>";
		echo "<h2  class=\"post-title\"><a href=\"\">Dem Pferdchen fiel kein Apfel auf den Kopf — Erfolglose Suche.</a></h2>";
		echo "<div class=\"content\"><p><img src=\"";
			bloginfo('template_directory');
		echo "/img/nosearch.png\" alt=\"Pony: „Huh — I'm not finding any search result.“\" /></p></div>";
		echo "</article>";
		echo "<br class=\"clear\" />";
	}
?>

<br class="clear" />
<section id="pagination">
	<div class="prev">
		<?php previous_posts_link('<span class="btn btn-white">vorherige Seite</span>'); ?>
	</div>
	<div class="next">
		<?php next_posts_link('<span class="btn btn-white">nächste Seite</span>'); ?>
	</div>
</section>

<?php get_footer(); ?>
